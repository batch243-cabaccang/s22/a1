/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [
  "James Jeffries",
  "Gunther Smith",
  "Macie West",
  "Michelle Queen",
  "Shane Miguelito",
  "Fernando Dela Cruz",
  "Akiko Yukihime",
];

let friendsList = ["DUMMY_FRIEND 1", "DUMMY_FRIEND 2"];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.
  
  */
const registerHandler = (input) => {
  const userExists = registeredUsers.includes(input);
  if (userExists) {
    alert("Registration failed. Username already exists!");
    return;
  } else if (!input) {
    alert("No name entered");
  }
  registeredUsers.push(input);
  alert("Thank you for registering!");
};

const fullName = prompt("Enter Full Name");
registerHandler(fullName);
console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.
  
  */

const addFriendHandler = (foundUser) => {
  const userExists = registeredUsers.includes(foundUser);
  if (userExists) {
    friendsList.push(foundUser);
    alert(`You have added ${foundUser} as a friend!`);
    return;
  }
  alert("User not found.");
};

const toBeAdded = prompt("Enter Username of Registered User");
addFriendHandler(toBeAdded);
console.log(friendsList);

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
  
  */

const displayFriends = () => {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
    return;
  }
  friendsList.forEach((friend) => {
    console.log(friend);
  });
};

displayFriends();

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function
  
  */

const numberOfFriends = () => {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
    return;
  }
  const totalCount = friendsList.length;
  alert(`You currently have ${totalCount} friend(s).`);
};

numberOfFriends();

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.
  
  */

const deleteLastFriend = () => {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
    return;
  }
  const deletedFriend = friendsList.pop();
  alert(`${deletedFriend} has been removed from your Friend List`);
};

deleteLastFriend();
console.log(friendsList);

/*
    Stretch Goal:
  
    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().
  
  */

// For Strech Goal

const deleteUseSplice = () => {
  const friendName = prompt("(For Strech Goal) Enter Friend's name to delete");
  const friendToDelete = friendsList.indexOf(friendName);
  if (friendToDelete === -1) {
    alert("Name is not on Friends List");
    return;
  }

  friendsList.splice(friendToDelete, 1);
  alert(`${friendName} has been deleted from your Friend List`);

  if (friendsList.length === 1) {
    alert("At least may isa ka pang friend. Wag mo na bitawan. Haha.");
  } else if (friendsList.length === 0) {
    alert(
      "Mag hanap ka na ulit ng friends mo para maging masaya ka naman. Haha"
    );
  }
  console.log(friendsList);
};

deleteUseSplice();
